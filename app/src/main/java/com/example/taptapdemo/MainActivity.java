package com.example.taptapdemo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import vn.com.truemoney.tmvsdk.*;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "MainActivity";
    static final String MERCHANT_CODE = "VUI-TAPTAP";
    static final String MERCHANT_KEY = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1UIAsn/mO/AV6q6Tr+aCfAtc8cvhJTNJLgGGOlgwPd3yY2raRKTZcTJhTlSFtivSP+1nZZN8Y3c4wiSsAs76urKkodK+I6IcX3B87JUP3p6YNwJQ3mqenxZTXcbaKw3nsKYf+Gmx0Nbf6RWy5T2HQKQkhwWMQf5nqmHyU8feihrKV3pzNAcDaJZGwc2UIi7RGufKN+8ocCyRk53dhYR+nqfSYGHZWj2tdA0HhlAL8RyJ53/1wHybH5qlvaZzIN9qFO/FgeXVul+y9C6Kwo8eJ4dJuAz8NnXczSrfFoJbFov3P4NHmJezyvdRhU6yqm2oW91oPdEkCeibKGTWXF+viwzA22rRkE7b+f950XvKrStWP2ZNLTiAZkrDZp8sVFgzjs7fhHSpFmz/VIfz04BpAQUdbu10nS7Q8kLtOnJnXuPD24/6ibe70BKwCxxAFa6XWIajr1qYwWFmxZgCQ2KosGpDweu11QxJFbI0lpQmC2bWjSlwhPexUwfh05I43bLboNbx5vYZXWmChAxaT9GDkeWvWMm9WpgK0QCLHz1I2A+KgTpJGibpjF17ZfKgVeoIIZproaunxgyqQtM/iqkMt3bevGKZYgUix7h8wWcL2kkudTVfKqkB5bJ8qokqs3BBCpHFu4MLTA2gzrgG9e8gg00X9CYeVn5Rw36CP1Uuh3MCAwEAAQ==";


    private Button checkPermissionButton;
    private Button linkButton;
    private Button unlinkButton;
    private Button gotoHomeButton;
    private TextView textView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermissionButton = findViewById(R.id.checkPermissionButton);
        linkButton = findViewById(R.id.linkButton);
        unlinkButton = findViewById(R.id.unlinkButton);
        textView = findViewById(R.id.userIdField);
        gotoHomeButton = findViewById(R.id.gotoHomeButton);
        progressBar = findViewById(R.id.progressBar);

        setupSDK();

        checkPermissionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doCheckPermission();
            }
        });


        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLink();
            }
        });

        gotoHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoHome();
            }
        });

        unlinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doUnlink();
            }
        });
    }

    private void setupSDK() {
        /// Init TMVInstance
        TMVInstance.init(this.getApplicationContext());

        /// Setup SDK
        TMVInstance.getInstance().setup(MERCHANT_CODE, MERCHANT_KEY, new TMVHandler() {
            @Override
            public void onSuccess(@Nullable Object result) {
                checkPermissionButton.setEnabled(true);
            }

            @Override
            public void onError(@Nullable TMVError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        /// Setup  deeplink callback
        if (getIntent().getData() != null) {
            TMVInstance.getInstance().handleDeeplink(getIntent().getData().toString());
        }
    }

    private  void doCheckPermission() {
        progressBar.setVisibility(View.VISIBLE);
        TMVInstance.getInstance().getAccessPermission(textView.getText().toString(), new TMVHandler() {
            @Override
            public void onSuccess(@Nullable Object result) {
                gotoHomeButton.setVisibility(View.VISIBLE);
                unlinkButton.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                checkPermissionButton.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(@Nullable TMVError error) {
                linkButton.setVisibility(View.VISIBLE);
                checkPermissionButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void doLink() {
        /// Link
        progressBar.setVisibility(View.VISIBLE);
        TMVInstance.getInstance().link(textView.getText().toString(), "vn.com.truemoney.taptapapp", new TMVHandler() {
            @Override
            public void onSuccess(@Nullable Object result) {
                progressBar.setVisibility(View.INVISIBLE);
                gotoHome();
            }

            @Override
            public void onError(@Nullable TMVError error) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG, "onError: " + error.toString());
            }
        });
    }

    private void doUnlink() {
        progressBar.setVisibility(View.VISIBLE);
        TMVInstance.getInstance().unlink(textView.getText().toString(), new TMVHandler() {

            @Override
            public void onSuccess(@Nullable Object result) {
                linkButton.setVisibility(View.VISIBLE);
                unlinkButton.setVisibility(View.INVISIBLE);
                gotoHomeButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(@Nullable TMVError error) {
                linkButton.setVisibility(View.VISIBLE);
                unlinkButton.setVisibility(View.INVISIBLE);
                gotoHomeButton.setVisibility(View.INVISIBLE);
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private  void gotoHome() {
        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        intent.putExtra("userId", textView.getText().toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
