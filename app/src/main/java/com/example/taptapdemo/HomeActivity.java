package com.example.taptapdemo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vn.com.truemoney.tmvsdk.TMVError;
import vn.com.truemoney.tmvsdk.TMVHandler;
import vn.com.truemoney.tmvsdk.TMVInstance;
import vn.com.truemoney.tmvsdk.WalletBalanceView;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";

    /// Hóa đơn
    private static final String qrType2 =
            "00020101021126240010A00000077501062000005204481453037045802VN5906VTVCab6005HANOI62430308VTVCab 106071616230070400010808Cap ADSL6304E7F3";
    /// Nhiều sản phẩm
    private static final String qrType3 =
            "00020101021126230006908405010911234384552044722530370454061000005802VN5915NguyetTran Shop6008DONGTHAP610600541162720320Nguyet Tran Shop CS10518020000000000787878070400010807den ngu0903AME6304ACCE";

    /// Type 1 tĩnh user phải nhập amount
    private static final String qrType1 =
            "0002010102112623000690840501091123438455204472253037045802VN5915NguyetTran Shop6008DONGTHAP610600541162280316Nguyet Shop CS 30704000363048B6A";

    /// Type 1 động có amount sẵn
    private static final String qrType1a =
            "00020101021226270010A00000077501091123438455204472253037045405100005802VN5915NguyetTran Shop6008DONGTHAP610600541162420106A123450320Nguyet Tran Shop CS1070400016304BE62";

    private String userId;

    private HashMap<String, String> qrTypes = new HashMap();
    private HashMap<String, Object> parsedQRObject;
    private double parsedAmount = 10000;
    private int parsedQRType = 1;

    private TextView verifyResultTextView;
    private TextView verifyDataTextView;
    private ProgressBar verifyProgressBar;
    private FrameLayout balanceFrameLayout;

    private TextView amountField;
    private TextView quantityField;
    private TextView transactionTextView;
    private ProgressBar transactionProgressBar;
    private WalletBalanceView balanceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        userId = getIntent().getStringExtra("userId");

        /// Demo QRTypes
        qrTypes.put("qrType1", qrType1);
        qrTypes.put("qrType1a", qrType1a);
        qrTypes.put("qrType2", qrType2);
        qrTypes.put("qrType3", qrType3);


        verifyDataTextView = findViewById(R.id.verifyDataTextView);
        verifyProgressBar = findViewById(R.id.verifyProgressBar);
        verifyResultTextView = findViewById(R.id.verifyResultTextView);
        balanceFrameLayout = findViewById(R.id.balanceFrameLayout);

        amountField = findViewById(R.id.amountTextField);
        quantityField = findViewById(R.id.quantityField);
        transactionProgressBar = findViewById(R.id.transactionProgressBar);
        transactionTextView = findViewById(R.id.transactionTextView);

        amountField.setEnabled(false);
        quantityField.setEnabled(true);

        showBalanceView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        balanceView.refreshBalance(userId, new TMVHandler() {
            @Override
            public void onSuccess(@Nullable Object result) {
                Toast.makeText(HomeActivity.this, "Fetched balance", Toast.LENGTH_SHORT);
            }

            @Override
            public void onError(@Nullable TMVError error) {
                Toast.makeText(HomeActivity.this, error.toString(), Toast.LENGTH_SHORT);
            }
        });
    }

    private void showBalanceView() {
        balanceView = TMVInstance.getInstance().getWalletBalanceView(this);
        balanceFrameLayout.addView(balanceView);
    }

    public void selectQRTypeClick(View view) {
        Button button = (Button) view;
        String selectedQRType = button.getText().toString();
        String qrTypeData = qrTypes.get(selectedQRType);

        TextView qrTextView = findViewById(R.id.verifyDataTextView);
        qrTextView.setText(qrTypeData);
    }

    public void verifyQRClick(View view) {
        doParseQRCode();
    }

    public void payClick(View view) {
        doPay();
    }

    private void doParseQRCode() {
        verifyProgressBar.setVisibility(View.VISIBLE);
        String qrInfor = verifyDataTextView.getText().toString();

        TMVInstance.getInstance().verifyQRCode(userId, qrInfor, new TMVHandler() {
            @Override
            public void onSuccess(@Nullable Object result) {
                verifyProgressBar.setVisibility(View.INVISIBLE);

                parsedQRObject = (HashMap<String, Object>) result;
                verifyResultTextView.setText(parsedQRObject.toString());

                amountField.setEnabled(false);
                quantityField.setEnabled(true);

                parsedQRType = Integer.valueOf((String) parsedQRObject.get("qr_type"));

                if (parsedQRType == 1) {
                    if (parsedQRObject.get("bill_number") != null) {
                        parsedAmount = (double) parsedQRObject.get("amount");
                        quantityField.setEnabled(false);
                    } else {
                        parsedAmount = 10000;
                        amountField.setEnabled(true);
                    }
                } else if (parsedQRType == 3) {
                    List<HashMap<String, Object>> productList = (ArrayList) parsedQRObject.get("product_list");
                    parsedAmount = Double.valueOf((String) productList.get(0).get("cost_price"));
                }

                amountField.setText(String.valueOf(parsedAmount));
            }

            @Override
            public void onError(@Nullable TMVError error) {
                verifyProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(HomeActivity.this, error.toString(), Toast.LENGTH_SHORT);
            }
        });
    }

    private void doPay() {
        transactionProgressBar.setVisibility(View.VISIBLE);

        List<HashMap<String, String>> qrItemList = new ArrayList();

        HashMap<String, String> qrItem = new HashMap();
        qrItem.put("quantity", quantityField.getText().toString());
        qrItem.put("note", "");
        qrItem.put("qrInfor", verifyDataTextView.getText().toString());

        qrItemList.add(qrItem);

        TMVInstance.getInstance().pay(userId, parsedQRType, parsedAmount, "", "", qrItemList, this, new TMVHandler() {
            @Override
            public void onSuccess(@Nullable Object result) {
                transactionProgressBar.setVisibility(View.INVISIBLE);
                transactionTextView.setText(result.toString());
            }

            @Override
            public void onError(@Nullable TMVError error) {
                transactionProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(HomeActivity.this, error.toString(), Toast.LENGTH_SHORT);
            }
        });
    }
}
